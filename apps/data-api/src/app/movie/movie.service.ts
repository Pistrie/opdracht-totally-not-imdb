import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Movie } from "./movie";
import { Comment } from "./comment";

@Injectable()
export class MovieService {
  constructor(@InjectModel("Movie") private movieModel: Model<Movie>) {}

  async createMovie(movie: Partial<Movie>): Promise<Movie> {
    const newMovie = new this.movieModel(movie);

    await newMovie.save();

    return newMovie.toObject({ versionKey: false });
  }

  async addCommentToMovie(
    movieId: string,
    comment: Partial<Comment>
  ): Promise<Movie> {
    comment.creationDate = new Date();
    comment.likes = 0;
    return this.movieModel.findOneAndUpdate(
      { _id: movieId },
      { $push: { comments: comment } },
      { new: true }
    );
  }

  async addLikeToMovie(movieId: string): Promise<Movie> {
    return this.movieModel.findOneAndUpdate(
      { _id: movieId },
      {
        $inc: {
          likes: 1,
        },
      },
      { new: true }
    );
  }

  async addLikeToComment(movieId: string, commentId: string): Promise<Movie> {
    return this.movieModel.findOneAndUpdate(
      { _id: movieId, "comments._id": commentId },
      {
        $inc: {
          "comments.$.likes": 1,
        },
      },
      { new: true }
    );
  }

  async getAll(): Promise<Movie[]> {
    return this.movieModel.find();
  }

  async getMovieById(id: string): Promise<Movie> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.movieModel.findById(id);
  }

  async updateMovie(id: string, changes: Partial<Movie>): Promise<Movie> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.movieModel.findOneAndUpdate({ _id: id }, changes, {
      new: true,
    });
  }

  async deleteMovie(id: string): Promise<Movie> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.movieModel.deleteOne({ _id: id });
  }

  async deleteCommentFromMovie(
    movieId: string,
    commentId: string
  ): Promise<Movie> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.movieModel.findOneAndUpdate(
      { _id: movieId },
      { $pull: { comments: { _id: commentId } } },
      { new: true }
    );
  }
}
