import { Test } from "@nestjs/testing";
import { MovieService } from "./movie.service";
import { MongoMemoryServer } from "mongodb-memory-server";
import { MongoClient } from "mongodb";
import { Movie } from "./movie";
import { disconnect, Model } from "mongoose";
import { getModelToken, MongooseModule } from "@nestjs/mongoose";
import { MovieSchema } from "./movie.schema";

describe("MovieService", () => {
  let service: MovieService;
  let mongod: MongoMemoryServer;
  let mongoc: MongoClient;
  let movieModel: Model<Movie>;

  const testMovies = [
    {
      _id: "6380a1313b9c2fc8e8c21b3f",
      name: "Pulp Fiction",
      directedBy: "Quentin Tarantino",
      starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
      releaseDate: new Date(1994),
      genre: ["Crime", "Drama"],
      ageRating: "A",
      imdbRating: 8.9,
      runningTime: 154,
      country: "",
      language: "",
      budget: 0,
      boxOffice: 107928762,
      comments: [],
      likes: 0,
    },
    {
      _id: "6380a1313b9c2fc8e8c21b56",
      name: "Pulp Reality",
      directedBy: "Quinten Smith",
      starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
      releaseDate: new Date(1949),
      genre: ["Romance", "Comedy"],
      ageRating: "PG",
      imdbRating: 7.0,
      runningTime: 154,
      country: "",
      language: "",
      budget: 0,
      boxOffice: 107928762,
      comments: [
        {
          body: "such a great movie",
          creationDate: new Date(),
          likes: 4,
          writtenBy: "6381f706087473500aef31f6",
          _id: "63907aac56afba82667175d2",
        },
      ],
      likes: 0,
    },
  ];

  beforeAll(async () => {
    let uri: string;

    const app = await Test.createTestingModule({
      imports: [
        MongooseModule.forRootAsync({
          useFactory: async () => {
            mongod = await MongoMemoryServer.create();
            uri = mongod.getUri();
            return { uri };
          },
        }),
        MongooseModule.forFeature([{ name: Movie.name, schema: MovieSchema }]),
      ],
      providers: [MovieService],
    }).compile();

    service = app.get<MovieService>(MovieService);
    movieModel = app.get<Model<Movie>>(getModelToken(Movie.name));

    mongoc = new MongoClient(uri);
    await mongoc.connect();
  });

  beforeEach(async () => {
    await mongoc.db("test").collection("movies").deleteMany({});

    const movie1 = new movieModel(testMovies[0]);
    const movie2 = new movieModel(testMovies[1]);

    await Promise.all([movie1.save(), movie2.save()]);
  });

  afterAll(async () => {
    await mongoc.close();
    await disconnect();
    await mongod.stop();
  });

  describe("createMovie", () => {
    it("should should create a movie", async function () {
      const result = await service.createMovie({
        name: "Pulp Reality",
        directedBy: "John Smith",
        releaseDate: new Date(),
      });

      expect(result).toHaveProperty("_id");
    });
  });

  describe("addCommentToMovie", () => {
    it("should add a comment to a movie", async function () {
      const result = await service.addCommentToMovie(testMovies[0]._id, {
        body: "such a great movie",
      });

      expect(result.comments[0]).toHaveProperty("_id");
    });
  });

  describe("addLikeToMovie", () => {
    it("should add a like to a movie", async function () {
      const result = await service.addLikeToMovie(testMovies[0]._id);

      expect(result.likes).toEqual(1);
    });
  });

  describe("addLikeToComment", () => {
    it("should add a like to a comment", async function () {
      const result = await service.addLikeToComment(
        testMovies[1]._id,
        testMovies[1].comments[0]._id
      );

      expect(result.comments[0].likes).toEqual(5);
    });
  });

  describe("getAll", () => {
    it("should retrieve all movies", async function () {
      const results = await service.getAll();

      expect(results).toHaveLength(2);
      expect(results.map((m) => m.name)).toContain("Pulp Fiction");
      expect(results.map((m) => m.name)).toContain("Pulp Reality");
    });
  });

  describe("getMovieById", () => {
    it("should retrieve a specific movie", async function () {
      const result = await service.getMovieById(testMovies[0]._id);

      expect(result).toHaveProperty("name", "Pulp Fiction");
    });

    it("should return null when no movie is found", async function () {
      const result = await service.getMovieById("6381f706087473500aef0000");

      expect(result).toBeNull();
    });
  });

  describe("updateMovie", () => {
    it("should update the movie", async function () {
      const result = await service.updateMovie(testMovies[0]._id, {
        name: "NEW MOVIE",
      });

      expect(result).toHaveProperty("name", "NEW MOVIE");
    });
  });

  describe("deleteMovie", () => {
    it("should delete the movie", async function () {
      const result = await service.deleteMovie(testMovies[0]._id);

      expect(result).toHaveProperty("deletedCount", 1);
    });
  });

  describe("deleteCommentFromMovie", () => {
    it("should delete the comment from the movie", async function () {
      const result = await service.deleteCommentFromMovie(
        testMovies[1]._id,
        testMovies[1].comments[0]._id
      );

      expect(result.comments).toHaveLength(0);
    });
  });
});
