import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UnauthorizedException,
  UseGuards,
} from "@nestjs/common";
import { MovieService } from "./movie.service";
import { Movie } from "./movie";
import { Comment } from "./comment";
import { AuthenticationGuard } from "../guards/authentication.guard";
import { AdminGuard } from "../guards/admin.guard";
import * as jwt from "jsonwebtoken";
import { User } from "../user/user";
import { UserService } from "../user/user.service";

@Controller("movie")
export class MovieController {
  constructor(
    private readonly movieService: MovieService,
    private readonly userService: UserService
  ) {}

  @Post()
  @UseGuards(AuthenticationGuard, AdminGuard)
  async createMovie(@Body() movie: Movie): Promise<Movie> {
    console.log("MovieController: createMovie called");

    return this.movieService.createMovie(movie);
  }

  @Post(":movieId/comment")
  @UseGuards(AuthenticationGuard)
  async addCommentToMovie(
    @Headers("authorization") authorization,
    @Param("movieId") movieId: string,
    @Body() comment: Comment
  ) {
    console.log("MovieController: addCommentToMovie called");

    const loggedInUser: User = jwt.verify(
      authorization,
      process.env.JWT_SECRET
    );
    comment.writtenBy = loggedInUser._id;

    return this.movieService.addCommentToMovie(movieId, comment);
  }

  @Post(":movieId/like")
  @UseGuards(AuthenticationGuard)
  async addLikeToMovie(@Param("movieId") movieId: string) {
    console.log("MovieController: addLikeToMovie called");

    return this.movieService.addLikeToMovie(movieId);
  }

  @Post(":movieId/comment/:commentId/like")
  @UseGuards(AuthenticationGuard)
  async addLikeToComment(
    @Param("movieId") movieId: string,
    @Param("commentId") commentId: string
  ) {
    console.log("MovieController: addLikeToComment called");

    return this.movieService.addLikeToComment(movieId, commentId);
  }

  @Get()
  async getAll(@Query("sortMovies") sortMovies?: string): Promise<Movie[]> {
    console.log("MovieController: getAll called");

    const movies = await this.movieService.getAll();

    if (sortMovies) {
      switch (sortMovies) {
        case "oldest":
          movies.sort(
            (a, b) => a.releaseDate.getTime() - b.releaseDate.getTime()
          );
          break;

        case "newest":
          movies.sort(
            (a, b) => b.releaseDate.getTime() - a.releaseDate.getTime()
          );
          break;

        case "likes":
          movies.sort((a, b) => b.likes - a.likes);
          break;

        default:
          throw new BadRequestException("incorrect sorting method supplied");
      }
    }

    return movies;
  }

  @Get(":movieId")
  async getMovieById(
    @Param("movieId") movieId: string,
    @Query("sortComments") sortComments?: string
  ): Promise<Movie> {
    console.log("MovieController: getMovieById called");

    const movie = await this.movieService.getMovieById(movieId);

    if (!movie) {
      throw new NotFoundException("Could not find movie with id: " + movieId);
    }

    if (sortComments) {
      switch (sortComments) {
        case "oldest":
          movie.comments.sort(
            (a, b) => a.creationDate.getTime() - b.creationDate.getTime()
          );
          break;

        case "newest":
          movie.comments.sort(
            (a, b) => b.creationDate.getTime() - a.creationDate.getTime()
          );
          break;

        case "likes":
          movie.comments.sort((a, b) => b.likes - a.likes);
          break;

        default:
          throw new BadRequestException("incorrect sorting method supplied");
      }
    }

    return movie;
  }

  @Put(":movieId")
  @UseGuards(AuthenticationGuard, AdminGuard)
  async updateMovie(@Param("movieId") movieId: string, @Body() changes: Movie) {
    console.log("MovieController: updateMovie called");

    if (changes._id) {
      throw new BadRequestException("Can't update movie id");
    }
    return this.movieService.updateMovie(movieId, changes);
  }

  @Delete(":movieId")
  @UseGuards(AuthenticationGuard, AdminGuard)
  async deleteMovie(@Param("movieId") movieId: string) {
    console.log("MovieController: deleteMovie called");

    return this.movieService.deleteMovie(movieId);
  }

  @Delete(":movieId/comment/:commentId")
  @UseGuards(AuthenticationGuard)
  async deleteCommentFromMovie(
    @Headers("authorization") authorization,
    @Param("movieId") movieId,
    @Param("commentId") commentId
  ) {
    console.log("MovieController: deleteCommentFromMovie called");

    const loggedInUser: User = jwt.verify(
      authorization,
      process.env.JWT_SECRET
    );
    const userInDb = await this.userService.getUserById(loggedInUser._id);
    const movieWithComment: Movie = await this.movieService.getMovieById(
      movieId
    );

    if (
      userInDb._id !==
        movieWithComment.comments.find((c) => c._id === commentId)?.writtenBy &&
      !loggedInUser.roles.includes("ADMIN")
    ) {
      throw new UnauthorizedException(
        "you're not the user who has written this comment"
      );
    }

    return this.movieService.deleteCommentFromMovie(movieId, commentId);
  }
}
