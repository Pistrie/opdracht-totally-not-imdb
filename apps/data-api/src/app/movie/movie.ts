import { IsArray, IsDate, IsInt, IsMongoId, IsString } from "class-validator";
import { Comment } from "./comment";

export class Movie {
  @IsString()
  @IsMongoId()
  _id: string;
  @IsString() name: string;
  @IsString() directedBy: string;
  @IsArray() starring: string[];
  @IsDate() releaseDate: Date;
  @IsArray() genre: string[];
  @IsString() ageRating: string;
  @IsInt() imdbRating: number;
  @IsInt() runningTime: number;
  @IsString() country: string;
  @IsString() language: string;
  @IsInt() budget: number;
  @IsInt() boxOffice: number;
  @IsArray() comments: Comment[];
  @IsInt() likes: number;
}
