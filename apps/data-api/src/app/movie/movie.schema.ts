import * as mongoose from "mongoose";

// query to replace comma seperated genres with array of genres
// db.movies.aggregate([{"$addFields":{"genre":{"$split":["$genre", ", "]}}},{"$out":"movies"}])

export const MovieSchema = new mongoose.Schema({
  name: String,
  directedBy: String,
  starring: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  ],
  releaseDate: Date,
  genre: [String],
  ageRating: String,
  imdbRating: Number,
  runningTime: Number,
  country: String,
  language: String,
  budget: Number,
  boxOffice: Number,
  comments: [
    {
      body: String,
      creationDate: Date,
      likes: Number,
      writtenBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    },
  ],
  likes: Number,
});
