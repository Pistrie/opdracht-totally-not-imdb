import { IsDate, IsInt, IsMongoId, IsString } from "class-validator";

export class Comment {
  @IsString()
  @IsMongoId()
  _id: string;
  @IsString() body: string;
  @IsDate() creationDate: Date;
  @IsInt() likes: number;
  @IsString() writtenBy: string;
}
