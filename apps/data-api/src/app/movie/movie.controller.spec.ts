import { Test, TestingModule } from "@nestjs/testing";
import { MovieService } from "./movie.service";
import { MovieController } from "./movie.controller";
import { UserService } from "../user/user.service";
import * as jwt from "jsonwebtoken";
import * as process from "process";
import { Movie } from "./movie";

describe("MovieController", () => {
  let app: TestingModule;
  let movieController: MovieController;
  let movieService: MovieService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [MovieController],
      providers: [
        {
          provide: MovieService,
          useValue: {
            createMovie: jest.fn(),
            addCommentToMovie: jest.fn(),
            addLikeToMovie: jest.fn(),
            addLikeToComment: jest.fn(),
            getAll: jest.fn(),
            getMovieById: jest.fn(),
            updateMovie: jest.fn(),
            deleteMovie: jest.fn(),
            deleteCommentFromMovie: jest.fn(),
          },
        },
        {
          provide: UserService,
          useValue: {},
        },
      ],
    }).compile();

    movieController = app.get<MovieController>(MovieController);
    movieService = app.get<MovieService>(MovieService);
  });

  describe("createMovie", () => {
    it("should call createMovie on the service", async () => {
      const exampleMovie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const createMovie = jest
        .spyOn(movieService, "createMovie")
        .mockImplementation(async () => exampleMovie);

      const result = await movieController.createMovie(exampleMovie);

      expect(createMovie).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleMovie._id);
    });
  });

  describe("addCommentToMovie", () => {
    it("should call addCommentToMovie on the service", async function () {
      const exampleComment = {
        _id: "1234",
        body: "such a great movie",
        creationDate: new Date(),
        likes: 0,
        writtenBy: "5678",
      };
      const exampleMovie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const addCommentToMovie = jest
        .spyOn(movieService, "addCommentToMovie")
        .mockImplementation(async () => exampleMovie);

      const authToken = jwt.sign(
        {
          _id: "1234567890",
          emailAddress: "example@email.com",
          roles: [],
        },
        process.env.JWT_SECRET
      );

      await movieController.addCommentToMovie(
        authToken,
        exampleMovie._id,
        exampleComment
      );

      expect(addCommentToMovie).toBeCalledTimes(1);
    });
  });

  describe("addLikeToMovie", () => {
    it("should call addLikeToMovie on the service", async function () {
      const exampleMovie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const addLikeToMovie = jest
        .spyOn(movieService, "addLikeToMovie")
        .mockImplementation(async () => exampleMovie);

      await movieController.addLikeToMovie(exampleMovie._id);

      expect(addLikeToMovie).toBeCalledTimes(1);
    });
  });

  describe("addLikeToComment", () => {
    it("should call addLikeToComment on the service", async function () {
      const exampleMovie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const addLikeToComment = jest
        .spyOn(movieService, "addLikeToComment")
        .mockImplementation(async () => exampleMovie);

      await movieController.addLikeToComment(exampleMovie._id, "1234");

      expect(addLikeToComment).toBeCalledTimes(1);
    });
  });

  describe("getAll", () => {
    it("should call getAll on the service", async function () {
      const exampleMovie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const getAll = jest
        .spyOn(movieService, "getAll")
        .mockImplementation(async () => [exampleMovie]);

      const results = await movieController.getAll();

      expect(getAll).toBeCalledTimes(1);
      expect(results).toHaveLength(1);
      expect(results[0]).toHaveProperty("_id", exampleMovie._id);
    });
  });

  describe("getMovieById", () => {
    it("should call getMovieById on the service", async function () {
      const exampleMovie: Movie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const getMovieById = jest
        .spyOn(movieService, "getMovieById")
        .mockImplementation(async () => exampleMovie);

      const result = await movieController.getMovieById(exampleMovie._id);

      expect(getMovieById).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleMovie._id);
    });
  });

  describe("updateMovie", () => {
    it("should call updateMovie on the service", async function () {
      const exampleMovie: Movie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };
      const exampleMovieChanges = {
        name: "Pulp Reality",
      };

      const updateMovie = jest
        .spyOn(movieService, "updateMovie")
        .mockImplementation(async () => exampleMovie);

      const result = await movieService.updateMovie(
        exampleMovie._id,
        exampleMovieChanges
      );

      expect(updateMovie).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleMovie._id);
    });
  });

  describe("deleteMovie", () => {
    it("should call deleteMovie on the service", async function () {
      const exampleMovie: Movie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const deleteMovie = jest
        .spyOn(movieService, "deleteMovie")
        .mockImplementation(async () => exampleMovie);

      const result = await movieService.deleteMovie(exampleMovie._id);

      expect(deleteMovie).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleMovie._id);
    });
  });

  describe("deleteCommentFromMovie", () => {
    it("should call deleteCommentFromMovie on the service", async function () {
      const exampleMovie: Movie = {
        _id: "1234",
        name: "Pulp Fiction",
        directedBy: "Quentin Tarantino",
        starring: ["6381f706087473500aef0000", "6381f79a087473500aef0000"],
        releaseDate: new Date(1994),
        genre: ["Crime", "Drama"],
        ageRating: "A",
        imdbRating: 8.9,
        runningTime: 154,
        country: "",
        language: "",
        budget: 0,
        boxOffice: 107928762,
        comments: [],
        likes: 0,
      };

      const deleteCommentFromMovie = jest
        .spyOn(movieService, "deleteCommentFromMovie")
        .mockImplementation(async () => exampleMovie);

      const result = await movieService.deleteCommentFromMovie(
        exampleMovie._id,
        "1234"
      );

      expect(deleteCommentFromMovie).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleMovie._id);
    });
  });
});
