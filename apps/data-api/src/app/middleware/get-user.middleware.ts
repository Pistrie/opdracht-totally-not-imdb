import { Injectable, NestMiddleware } from "@nestjs/common";
import * as jwt from "jsonwebtoken";

@Injectable()
export class GetUserMiddleware implements NestMiddleware {
  use(req: any, res: any, next: (error?: any) => void): any {
    const authJwt = req.headers.authorization;

    if (!authJwt) {
      console.log("no JWT found");
      next();
      return;
    }

    try {
      const user = jwt.verify(authJwt, process.env.JWT_SECRET);
      if (user) {
        console.log("found user details in JWT: " + JSON.stringify(user));
        req["user"] = user;
      }
    } catch (e) {
      console.log("error handling authentication JWT: " + e.message);
    }

    next();
    return;
  }
}
