import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  NotFoundException,
  Param,
  Post,
  Put,
  UnauthorizedException,
  UseGuards,
} from "@nestjs/common";
import { UserService } from "./user.service";
import { User } from "./user";
import { AuthenticationGuard } from "../guards/authentication.guard";
import { AdminGuard } from "../guards/admin.guard";
import * as jwt from "jsonwebtoken";

@Controller("user")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @UseGuards(AuthenticationGuard, AdminGuard)
  async createUser(@Body() user: User): Promise<User> {
    console.log("UserController: adding user...");
    return this.userService.createUser(user);
  }

  @Get()
  @UseGuards(AuthenticationGuard, AdminGuard)
  async getAll(): Promise<User[]> {
    console.log("UserController: getting all users...");
    return this.userService.getAll();
  }

  @Get(":userId")
  async getUserById(@Param("userId") userId: string): Promise<User> {
    console.log("UserController: getting single user...");
    const user = await this.userService.getUserById(userId);

    if (!user) {
      throw new NotFoundException("Could not find user with id: " + userId);
    }
    return user;
  }

  @Get("actor/colleagues")
  async getAllColleaguesOfActor(@Body("actorName") actor: string) {
    console.log("userController: getAllColleaguesOfActor called");

    return this.userService.getAllColleaguesOfActor(actor);
  }

  @Put(":userId")
  @UseGuards(AuthenticationGuard)
  async updateUser(
    @Headers("authorization") authorization,
    @Param("userId") userId: string,
    @Body() changes: User
  ): Promise<User> {
    console.log("UserController: updating user...");

    const user = jwt.verify(authorization, process.env.JWT_SECRET);
    const userInDb = await this.userService.getUserById(user._id);

    if (userInDb == null) {
      throw new UnauthorizedException();
    }

    if (changes._id) {
      throw new BadRequestException("Can't update user id");
    }
    return this.userService.updateUser(userId, changes);
  }

  @Delete(":userId")
  @UseGuards(AuthenticationGuard)
  async deleteUser(
    @Headers("authorization") authorization,
    @Param("userId") userId: string
  ) {
    console.log("UserController: deleting user...");

    const user = jwt.verify(authorization, process.env.JWT_SECRET);
    const userInDb = await this.userService.getUserById(user._id);

    if (userInDb == null) {
      throw new UnauthorizedException();
    }
    return this.userService.deleteUser(userId);
  }

  @Put(":userId/friend/:friendId")
  @UseGuards(AuthenticationGuard)
  async addFriend(
    @Headers("authorization") authorization,
    @Param("userId") userId: string,
    @Param("friendId") friendId: string
  ) {
    console.log("UserController: adding friendId...");

    const loggedInUser: User = jwt.verify(
      authorization,
      process.env.JWT_SECRET
    );
    const userInDb = await this.userService.getUserById(userId);

    if (
      loggedInUser.emailAddress !== userInDb.emailAddress &&
      !loggedInUser.roles.includes("ADMIN")
    ) {
      throw new UnauthorizedException(
        "you're not the user you're trying to edit"
      );
    }

    if (userId === friendId) {
      throw new BadRequestException("Can't add yourself as a friend");
    }
    return this.userService.addFriend(userId, friendId);
  }

  @Delete(":userId/friend/:friendId")
  @UseGuards(AuthenticationGuard)
  async removeFriend(
    @Headers("authorization") authorization,
    @Param("userId") userId: string,
    @Param("friendId") friendId: string
  ) {
    console.log("UserController: removing friendId...");

    const loggedInUser: User = jwt.verify(
      authorization,
      process.env.JWT_SECRET
    );
    const userInDb = await this.userService.getUserById(userId);

    if (
      loggedInUser.emailAddress !== userInDb.emailAddress &&
      !loggedInUser.roles.includes("ADMIN")
    ) {
      throw new UnauthorizedException(
        "you're not the user you're trying to edit"
      );
    }

    if (userId === friendId) {
      throw new UnauthorizedException(
        "you're not the user you're trying to edit"
      );
    }
    return this.userService.removeFriend(userId, friendId);
  }
}
