import { Movie } from "../movie/movie";
import { IsArray, IsDate, IsString } from "class-validator";

export class Actor {
  @IsString() name: string;
  @IsString() country: string;
  @IsDate() activeFrom: Date;
  @IsDate() activeUntil: Date;
  @IsArray() movieAppearances: Movie[];
}
