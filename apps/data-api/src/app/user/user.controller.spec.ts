import { Test, TestingModule } from "@nestjs/testing";
import { UserController } from "./user.controller";
import { UserService } from "./user.service";
import { User } from "./user";
import * as jwt from "jsonwebtoken";

describe("UserController", () => {
  let app: TestingModule;
  let userController: UserController;
  let userService: UserService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: UserService,
          useValue: {
            createUser: jest.fn(),
            getAll: jest.fn(),
            getUserById: jest.fn(),
            updateUser: jest.fn(),
            deleteUser: jest.fn(),
            addFriend: jest.fn(),
            deleteFriend: jest.fn(),
          },
        },
      ],
    }).compile();

    userController = app.get<UserController>(UserController);
    userService = app.get<UserService>(UserService);
  });

  describe("createUser", () => {
    it("should call createUser on the service", async () => {
      const exampleUser = {
        _id: "1234",
        username: "pistrie",
        password: "password",
        emailAddress: "sylvester.roos@xs4all.nl",
        roles: [],
        creationDate: new Date(),
        actor: null,
        dateOfBirth: new Date(2000, 11, 23),
        friends: [],
      };

      const createUser = jest
        .spyOn(userService, "createUser")
        .mockImplementation(async () => exampleUser);

      const result = await userController.createUser(exampleUser);
      // console.log(JSON.stringify(result));

      expect(createUser).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleUser._id);
    });
  });

  describe("getAll", () => {
    it("should call getAll on the service", async () => {
      const exampleUser: User = {
        _id: "1234",
        username: "pistrie",
        password: "password",
        emailAddress: "sylvester.roos@xs4all.nl",
        roles: [],
        creationDate: new Date(),
        actor: null,
        dateOfBirth: new Date(2000, 11, 23),
        friends: [],
      };

      const getAll = jest
        .spyOn(userService, "getAll")
        .mockImplementation(async () => [exampleUser]);

      const results = await userController.getAll();
      // console.log(JSON.stringify(results));

      expect(getAll).toBeCalledTimes(1);
      expect(results).toHaveLength(1);
      expect(results[0]).toHaveProperty("_id", exampleUser._id);
    });
  });

  describe("getUserById", () => {
    it("should call getUserById on the service with id from parameter", async () => {
      const exampleUser: User = {
        _id: "1234",
        username: "pistrie",
        password: "password",
        emailAddress: "sylvester.roos@xs4all.nl",
        roles: [],
        creationDate: new Date(),
        actor: null,
        dateOfBirth: new Date(2000, 11, 23),
        friends: [],
      };

      const getUserById = jest
        .spyOn(userService, "getUserById")
        .mockImplementation(async () => exampleUser);

      const result = await userController.getUserById(exampleUser._id);
      // console.log(JSON.stringify(result));

      expect(getUserById).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleUser._id);
    });
  });

  describe("updateUser", () => {
    it("should call updateUser on the service with id and changes from parameters", async () => {
      const exampleUser: User = {
        _id: "1234",
        username: "pistrie",
        password: "password",
        emailAddress: "sylvester.roos@xs4all.nl",
        roles: [],
        creationDate: new Date(),
        actor: null,
        dateOfBirth: new Date(2000, 11, 23),
        friends: [],
      };
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const exampleUserChanges: User = {
        username: "pistrie1234",
      };

      const updateUser = jest
        .spyOn(userService, "updateUser")
        .mockImplementation(async () => exampleUser);

      const authToken = jwt.sign(
        {
          _id: exampleUser._id,
          emailAddress: exampleUser.emailAddress,
          roles: [],
        },
        process.env.JWT_SECRET
      );

      const result = await userController.updateUser(
        authToken,
        exampleUser._id,
        exampleUserChanges
      );
      // console.log(JSON.stringify(result));

      expect(updateUser).toBeCalledTimes(1);
      expect(result).toHaveProperty("_id", exampleUser._id);
    });
  });
});
