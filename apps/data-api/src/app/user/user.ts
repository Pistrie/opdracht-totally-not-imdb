import { IsArray, IsDate, IsMongoId, IsString } from "class-validator";
import { Actor } from "./actor";

export class User {
  @IsString()
  @IsMongoId()
  _id: string;
  @IsString() username: string;
  @IsString() password: string;
  @IsString() emailAddress: string;
  @IsArray() roles: string[];
  @IsDate() creationDate: Date;
  actor: Actor;
  @IsDate() dateOfBirth: Date;
  @IsArray() friends: string[];
}
