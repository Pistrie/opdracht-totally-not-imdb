import * as mongoose from "mongoose";

export const UserSchema = new mongoose.Schema({
  username: String,
  password: String,
  emailAddress: String,
  roles: Array,
  creationDate: Date,
  actor: {
    name: String,
    country: String,
    activeFrom: Date,
    activeUntil: Date,
  },
  DateOfBirth: Date,
  friends: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  ],
});
