import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { Model } from "mongoose";
import { User } from "./user";
import { Neo4jService } from "../neo4j/neo4j.service";

@Injectable()
export class UserService {
  constructor(
    @InjectModel("User") private userModel: Model<User>,
    private neo4jService: Neo4jService
  ) {}

  async createUser(user: Partial<User>): Promise<User> {
    const newUser = new this.userModel(user);

    await newUser.save();

    return newUser.toObject({ versionKey: false });
  }

  async getAll(): Promise<User[]> {
    return this.userModel.find();
  }

  getUserById(id: string): Promise<User> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.userModel.findById(id);
  }

  async getAllColleaguesOfActor(actorName: string): Promise<string[]> {
    const neoQuery = `
match (actor:Person)-[:ACTED_IN]->(movie:Movie)<-[:ACTED_IN]-(colleague:Person)
where actor.name = $actorName
return colleague
    `;
    const colleaguesFromActor = await this.neo4jService.singleRead(neoQuery, {
      actorName,
    });

    const colleagues = [];
    colleaguesFromActor.records.forEach((record) => {
      colleagues.push(record.get("colleague").properties.name);
    });

    return colleagues;
  }

  updateUser(id: string, changes: Partial<User>): Promise<User> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.userModel.findOneAndUpdate({ _id: id }, changes, {
      new: true,
    });
  }

  deleteUser(id: string) {
    return this.userModel.deleteOne({ _id: id });
  }

  addFriend(id: string, friendId: string) {
    return this.userModel.findOneAndUpdate(
      { _id: id },
      { $push: { friends: new mongoose.Types.ObjectId(friendId) } },
      { new: true }
    );
  }

  removeFriend(id: string, friendId: string) {
    return this.userModel.findOneAndUpdate(
      { _id: id },
      { $pull: { friends: new mongoose.Types.ObjectId(friendId) } },
      { new: true }
    );
  }
}
