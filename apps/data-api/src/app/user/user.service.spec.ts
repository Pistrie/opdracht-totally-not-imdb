import { UserService } from "./user.service";
import { MongoMemoryServer } from "mongodb-memory-server";
import { MongoClient } from "mongodb";
import { getModelToken, MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "./user.schema";
import * as mongoose from "mongoose";
import { disconnect, Model } from "mongoose";
import { User } from "./user";
import { Test } from "@nestjs/testing";

describe("UserService", () => {
  let service: UserService;
  let mongod: MongoMemoryServer;
  let mongoc: MongoClient;
  let userModel: Model<User>;

  const testUsers = [
    {
      _id: "6381f706087473500aef31f6",
      username: "sylvester",
      password: "password",
      emailAddress: "example@emaill.com",
      roles: [],
      creationDate: new Date(),
      dateOfBirth: new Date(2000, 11, 23),
      friends: ["6381f79a087473500aef31f8"],
    },
    {
      _id: "6381f79a087473500aef31f8",
      username: "dinand",
      password: "password",
      emailAddress: "example@emaill.com",
      roles: [],
      creationDate: new Date(),
      dateOfBirth: new Date(2004, 2, 13),
      friends: [],
    },
  ];

  beforeAll(async () => {
    let uri: string;

    const app = await Test.createTestingModule({
      imports: [
        MongooseModule.forRootAsync({
          useFactory: async () => {
            mongod = await MongoMemoryServer.create();
            uri = mongod.getUri();
            return { uri };
          },
        }),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
      ],
      providers: [UserService],
    }).compile();

    service = app.get<UserService>(UserService);
    userModel = app.get<Model<User>>(getModelToken(User.name));

    mongoc = new MongoClient(uri);
    await mongoc.connect();
  });

  beforeEach(async () => {
    await mongoc.db("test").collection("users").deleteMany({});

    const user1 = new userModel(testUsers[0]);
    const user2 = new userModel(testUsers[1]);

    await Promise.all([user1.save(), user2.save()]);
  });

  afterAll(async () => {
    await mongoc.close();
    await disconnect();
    await mongod.stop();
  });

  describe("createUser", () => {
    it("should create a user", async () => {
      const result = await service.createUser({
        username: "new user",
        password: "password",
        emailAddress: "test@email.com",
      });

      expect(result).toHaveProperty("_id");
    });
  });

  describe("getAll", () => {
    it("should retrieve all users", async function () {
      const results = await service.getAll();

      console.log(JSON.stringify(results[1]));

      expect(results).toHaveLength(2);
      expect(results.map((r) => r.username)).toContain("sylvester");
      expect(results.map((r) => r.username)).toContain("dinand");
    });
  });

  describe("getUserById", () => {
    it("should retrieve a specific user", async function () {
      const result = await service.getUserById("6381f706087473500aef31f6");

      expect(result).toHaveProperty("username", "sylvester");
    });

    it("should return null when user is not found", async function () {
      const result = await service.getUserById("6381f706087473500aef0000");

      expect(result).toBeNull();
    });
  });

  describe("updateUser", () => {
    it("should update the user", async function () {
      const result = await service.updateUser(testUsers[0]._id, {
        username: "sylvester roos",
      });

      expect(result).toHaveProperty("username", "sylvester roos");
    });
  });

  describe("deleteUser", () => {
    it("should delete the user", async function () {
      const result = await service.deleteUser(testUsers[0]._id);

      expect(result).toHaveProperty("deletedCount", 1);
    });
  });

  describe("addFriend", () => {
    it("should add a user as a friend", async function () {
      const result = await service.addFriend(
        testUsers[1]._id,
        testUsers[0]._id
      );

      expect(result.friends).toContainEqual(
        new mongoose.Types.ObjectId(testUsers[0]._id)
      );
      expect(result.friends).toHaveLength(1);
    });
  });

  describe("removeFriend", () => {
    it("should remove a user as a friend", async function () {
      const result = await service.removeFriend(
        testUsers[0]._id,
        testUsers[1]._id
      );

      expect(result.friends).toHaveLength(0);
    });
  });
});
