import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from "@nestjs/common";
import { Observable } from "rxjs";

@Injectable()
export class AuthorizationGuard implements CanActivate {
  constructor(private allowedRoles: string[]) {}

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const host = context.switchToHttp();
    const request = host.getRequest();

    const user = request["user"];

    const allowed = this.isAllowed(user.roles);

    console.log("user is allowed", allowed);

    if (!allowed) {
      console.log(
        "user is authenticated but not authorized, denying access..."
      );
      throw new ForbiddenException();
    }

    console.log("user is authorized, allowing access...");
    return true;
  }

  isAllowed(userRoles: string[]): boolean {
    console.log("comparing roles: " + this.allowedRoles, userRoles);

    let allowed = false;

    userRoles.forEach((role) => {
      console.log("checking if role is allowed: " + role);

      if (!allowed && this.allowedRoles.includes(role)) {
        allowed = true;
      }
    });

    return allowed;
  }
}
