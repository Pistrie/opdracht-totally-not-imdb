import { Body, Controller, Post, UnauthorizedException } from "@nestjs/common";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { User } from "../user/user";
import * as password from "password-hash-and-salt";
import * as jwt from "jsonwebtoken";

@Controller("login")
export class AuthController {
  constructor(@InjectModel("User") private userModel: Model<User>) {}

  @Post()
  async login(
    @Body("email") emailAddress: string,
    @Body("password") plaintextPassword: string
  ) {
    const user = await this.userModel.findOne({ emailAddress: emailAddress });

    if (!user) {
      console.log("User does not exist");
      throw new UnauthorizedException();
    }

    return new Promise((resolve, reject) => {
      password(plaintextPassword).verifyAgainst(
        user.password,
        (err, verified) => {
          if (!verified) {
            reject(new UnauthorizedException());
          }

          const authJwtToken = jwt.sign(
            { _id: user.id, emailAddress, roles: user.roles },
            process.env.JWT_SECRET
          );

          resolve({
            authJwtToken,
          });
        }
      );
    });
  }
}
