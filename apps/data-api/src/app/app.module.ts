import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { DataModule } from "./data.module";
import { AuthModule } from "./auth.module";
import { GetUserMiddleware } from "./middleware/get-user.middleware";
import { MovieController } from "./movie/movie.controller";
import { UserController } from "./user/user.controller";
import { Neo4jModule } from "./neo4j/neo4j.module";

@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb+srv://${process.env.MONGO_USR}:${process.env.MONGO_PWD}@${process.env.MONGO_HOST}/${process.env.MONGO_DATABASE}?retryWrites=true&w=majority`
    ),
    Neo4jModule.forRoot({
      scheme: process.env.NEO4J_SCHEME,
      host: process.env.NEO4J_HOST,
      username: process.env.NEO4J_USR,
      password: process.env.NEO4J_PWD,
      database: process.env.NEO4J_DATABASE,
    }),
    DataModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer
      .apply(GetUserMiddleware)
      .forRoutes(MovieController, UserController);
  }
}
