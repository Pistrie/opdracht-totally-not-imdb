import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Movie } from "./movie/movie";
import { MovieSchema } from "./movie/movie.schema";
import { MovieController } from "./movie/movie.controller";
import { MovieService } from "./movie/movie.service";
import { UserController } from "./user/user.controller";
import { UserService } from "./user/user.service";
import { UserSchema } from "./user/user.schema";
import { User } from "./user/user";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Movie.name, schema: MovieSchema },
      { name: User.name, schema: UserSchema },
    ]),
  ],
  controllers: [MovieController, UserController],
  providers: [MovieService, UserService],
})
export class DataModule {}
