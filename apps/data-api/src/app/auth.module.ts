import { Module } from "@nestjs/common";
import { AuthController } from "./auth/auth.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "./user/user.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: "User",
        schema: UserSchema,
      },
    ]),
  ],
  controllers: [AuthController],
  providers: [],
})
export class AuthModule {}
